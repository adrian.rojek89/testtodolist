﻿using DatabaseContext.Model;
using DatabaseContext.Repository;
using System.Collections.Generic;
using System.Windows.Forms;
using TestToDoList.Controls;
using TestToDoList.View;

namespace TestToDoList
{
    public class ToDoListViewModel
    {
        public delegate void SpawnUserTaskDelegate(UserTask userTask);
        public SpawnUserTaskDelegate CreateUserTaskUnderCategory;

        private ICategoryRepository _categoryRepository = new CategoryRepository();
        private IUserTaskRepository _userTaskRepository = new UserTaskRepository();

        public List<Category> GetAllCategories()
        {
            return _categoryRepository.GetAll();
        }

        public bool CreateCategory(Category category)
        {
            return _categoryRepository.Create(category);
        }

        public bool DeleteCategory(int id)
        {
            return _categoryRepository.Delete(id);
        }

        public bool UpdateCategory(Category category)
        {
            return _categoryRepository.Update(category);
        }

        public bool UpdateCategoryPosition(List<Category> categories)
        {
            return _categoryRepository.UpdatePosition(categories);
        }

        public UserTask GetUserTaskById(int id)
        {
            return _userTaskRepository.GetById(id);
        }

        public bool CreateUserTask(UserTask userTask)
        {
            return _userTaskRepository.Create(userTask);
        }

        public bool DeleteUserTask(int id)
        {
            return _userTaskRepository.Delete(id);
        }

        public bool UpdateUserTask(UserTask userTask)
        {
            return _userTaskRepository.Update(userTask);
        }

        public void CreateNewUserTaskControl(Control parent, int categoryId)
        {
            UserTask newUserTask = new UserTask();
            newUserTask.CategoryId = categoryId;

            CreateUserTask createUserTask = new CreateUserTask(newUserTask);

            if (createUserTask.ShowDialog() == DialogResult.OK)
            {
                CreateUserTask(newUserTask);
                CreateUserTaskControl(newUserTask, parent);
            }
        }

        public void CreateUserTaskControl(UserTask userTask, Control parent)
        {
            UserTaskControl userTaskControl = new UserTaskControl(userTask, this);
            userTaskControl.Dock = DockStyle.Top;

            parent.Controls.Add(userTaskControl);
        }

        public void CreateNewCategoryControl(Control parent)
        {
            Category newCategory = new Category();

            CreateCategory createCategory = new CreateCategory(newCategory);

            if (createCategory.ShowDialog() == DialogResult.OK)
            {
                newCategory.PlaceId = GetPlaceIdOfLastCategory(parent);
                CreateCategory(newCategory);
                CreateCategoryControl(newCategory, parent);
            }
        }

  
        private int GetPlaceIdOfLastCategory(Control parent)
        {
            int countOfControls = parent.Controls.Count;
            if (countOfControls > 0)
            {
                CategoryControl control = parent.Controls[countOfControls - 1] as CategoryControl;
                return control.PlaceId + 1;
            }
            else
            {
                return 0;
            }
        }

        public void CreateCategoryControl(Category category, Control parent)
        {
            CategoryControl control = new CategoryControl(category, this);
            parent.Controls.Add(control);
        }
    }
}
