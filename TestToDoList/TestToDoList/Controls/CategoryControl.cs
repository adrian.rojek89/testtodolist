﻿using DatabaseContext.Model;
using System.Windows.Forms;
using System.Linq;

namespace TestToDoList.Controls
{
    public partial class CategoryControl : UserControl
    {
        public int PlaceId => _category.PlaceId;
        public int CategoryId => _category.Id;

        private Category _category;
        private ToDoListViewModel _mainViewModel;

        public CategoryControl(Category category, ToDoListViewModel mainViewModel)
        {
            InitializeComponent();
            _category = category;
            _mainViewModel = mainViewModel;
            UpdateView();
        }

        public void DeleteUserTaskControl(UserTask userTask)
        {
            UserTaskControl userTaskControl =
                pTaskContainer.Controls.OfType<UserTaskControl>().FirstOrDefault(t => t.GetUserTaskId() == userTask.Id);

            if(userTaskControl != null)
            {
                pTaskContainer.Controls.Remove(userTaskControl);
            }
        }

        public void CreateUserTaskUnderThisCategory(UserTask userTask)
        {
            _mainViewModel.CreateUserTaskControl(userTask, pTaskContainer);
        }

        private void UpdateView()
        {
            lblCategoryName.Text = _category.Name;
            pTaskContainer.Controls.Clear();

            if(_category.UserTasks != null)
            {
                foreach (UserTask userTask in _category.UserTasks)
                {
                    _mainViewModel.CreateUserTaskControl(userTask, pTaskContainer);
                }
            }
        }

        private void btnCreateTask_Click(object sender, System.EventArgs e)
        {
            _mainViewModel.CreateNewUserTaskControl(pTaskContainer, _category.Id);
        }
    }
}
