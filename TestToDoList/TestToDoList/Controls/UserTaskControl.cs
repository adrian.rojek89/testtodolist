﻿using DatabaseContext.Model;
using System;
using System.Drawing;
using System.Windows.Forms;
using TestToDoList.View;

namespace TestToDoList.Controls
{
    public partial class UserTaskControl : UserControl
    {
        private ToDoListViewModel _toDoListViewModel;
        private UserTask _userTask;
        private int _oldCategoryId = 0;

        public UserTaskControl(UserTask userTask, ToDoListViewModel toDoListViewModel)
        {
            InitializeComponent();
            _userTask = userTask;
            _toDoListViewModel = toDoListViewModel;
            _oldCategoryId = _userTask.CategoryId;
            UpdateView();
        }

        public int GetUserTaskId() => _userTask.Id;

        private void UpdateView()
        {
            lblTaskName.Text = _userTask.Name;
            tbDescription.Text = _userTask.Description;
        }

        private void UserTask_MouseLeave(object sender, EventArgs e)
        {
            BackColor = Color.PaleTurquoise;
        }

        private void UserTask_MouseEnter(object sender, EventArgs e)
        {
            BackColor = Color.White;
        }

        private void UserTaskControl_DoubleClick(object sender, EventArgs e)
        {
            EditUserTask editUserTask = new EditUserTask(_userTask, _toDoListViewModel.GetAllCategories());

            if (editUserTask.ShowDialog() == DialogResult.OK)
            {
                ClosedEditView(editUserTask);
            }
        }

        private void ClosedEditView(EditUserTask editUserTask)
        {
            if (editUserTask.Result == EditUserTask.RESULT.Delete)
            {
                _toDoListViewModel.DeleteUserTask(_userTask.Id);
                DeleteUserTask();
            }
            else if (editUserTask.Result == EditUserTask.RESULT.Edit)
            {
                _toDoListViewModel.UpdateUserTask(_userTask);
                if(_oldCategoryId != _userTask.CategoryId)
                {
                    _oldCategoryId = _userTask.CategoryId;
                    _toDoListViewModel.CreateUserTaskUnderCategory(_userTask);
                    DeleteUserTask();
                }
                else
                {
                    UpdateView();
                }
            }
        }

        private CategoryControl GetParentCategoryControl()
        {
            CategoryControl categoryControl = Parent as CategoryControl;

            if(categoryControl == null)
            {
                categoryControl = Parent.Parent as CategoryControl;
            }

            return categoryControl;
        }

        private void DeleteUserTask()
        {
            CategoryControl categoryControl = GetParentCategoryControl();

            if (categoryControl != null)
            {
                categoryControl.DeleteUserTaskControl(_userTask);
            }
        }
    }
}
