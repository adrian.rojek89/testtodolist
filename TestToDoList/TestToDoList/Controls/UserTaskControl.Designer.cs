﻿namespace TestToDoList.Controls
{
    partial class UserTaskControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTaskName = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTaskName
            // 
            this.lblTaskName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTaskName.Location = new System.Drawing.Point(4, 4);
            this.lblTaskName.Name = "lblTaskName";
            this.lblTaskName.Size = new System.Drawing.Size(174, 42);
            this.lblTaskName.TabIndex = 0;
            this.lblTaskName.Text = "Task Name";
            this.lblTaskName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTaskName.DoubleClick += new System.EventHandler(this.UserTaskControl_DoubleClick);
            this.lblTaskName.MouseEnter += new System.EventHandler(this.UserTask_MouseEnter);
            this.lblTaskName.MouseLeave += new System.EventHandler(this.UserTask_MouseLeave);
            // 
            // tbDescription
            // 
            this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDescription.Enabled = false;
            this.tbDescription.Location = new System.Drawing.Point(4, 49);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.ReadOnly = true;
            this.tbDescription.Size = new System.Drawing.Size(172, 127);
            this.tbDescription.TabIndex = 1;
            // 
            // UserTaskControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.lblTaskName);
            this.Margin = new System.Windows.Forms.Padding(3, 20, 3, 10);
            this.Name = "UserTaskControl";
            this.Size = new System.Drawing.Size(179, 179);
            this.DoubleClick += new System.EventHandler(this.UserTaskControl_DoubleClick);
            this.MouseEnter += new System.EventHandler(this.UserTask_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.UserTask_MouseLeave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.TextBox tbDescription;
    }
}
