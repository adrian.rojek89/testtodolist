﻿using DatabaseContext.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace TestToDoList.View
{
    public partial class CreateCategory : Form
    {
        Category _newCategory;

        public CreateCategory(Category newCategory)
        {
            InitializeComponent();
            _newCategory = newCategory;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(tbCategoryName.Text))
            {
                _newCategory.Name = tbCategoryName.Text;

                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                lblName.BackColor = Color.Red;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel; 
            Close();
        }
    }
}
