﻿using DatabaseContext.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TestToDoList.View
{
    public partial class EditUserTask : Form
    {
        public enum RESULT
        {
            Edit,
            Delete,
            Cancel
        }

        public RESULT Result = RESULT.Cancel;
        private UserTask _userTask;

        public EditUserTask(UserTask userTask, List<Category> categorys)
        {
            InitializeComponent();
            _userTask = userTask;

            var bindingSource1 = new BindingSource();
            bindingSource1.DataSource = categorys;

            cbCategory.DataSource = bindingSource1.DataSource;
            cbCategory.DisplayMember = "Name";
            cbCategory.ValueMember = "Id";

            UpdateView();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsFormValidate())
            {
                _userTask.Name = tbName.Text;
                _userTask.Description = tbDescription.Text;
                _userTask.CategoryId = (int)cbCategory.SelectedValue;

                DialogResult = DialogResult.OK;
                Result = RESULT.Edit;
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Result = RESULT.Cancel;
            Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Result = RESULT.Delete;
            Close();
        }

        private bool IsFormValidate()
        {
            lblName.ForeColor = Color.Black;
            lblDescription.ForeColor = Color.Black;

            return ValidateForm();
        }

        private bool ValidateForm()
        {
            bool formIsValidate = true;

            if (string.IsNullOrEmpty(tbName.Text))
            {
                lblName.ForeColor = Color.Red;
                formIsValidate = false;
            }

            if (string.IsNullOrEmpty(tbDescription.Text))
            {
                lblDescription.ForeColor = Color.Red;
                formIsValidate = false;
            }

            return formIsValidate;
        }

        private void UpdateView()
        {
            tbName.Text = _userTask.Name;
            tbDescription.Text = _userTask.Description;

            cbCategory.SelectedValue = _userTask.CategoryId;
        }

        
    }
}
