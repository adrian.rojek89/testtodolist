﻿using DatabaseContext.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace TestToDoList.View
{
    public partial class CreateUserTask : Form
    {
        UserTask _newUserTask;

        public CreateUserTask(UserTask newUserTask)
        {
            InitializeComponent();
            _newUserTask = newUserTask;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsFormValidate())
            {
                _newUserTask.Name = tbName.Text;
                _newUserTask.Description = tbDescription.Text;

                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private bool IsFormValidate()
        {
            lblName.ForeColor = Color.Black;
            lblDescription.ForeColor = Color.Black;

            return ValidateForm();
        }

        private bool ValidateForm()
        {
            bool formIsValidate = true;

            if (string.IsNullOrEmpty(tbName.Text))
            {
                lblName.ForeColor = Color.Red;
                formIsValidate = false;
            }

            if (string.IsNullOrEmpty(tbDescription.Text))
            {
                lblDescription.ForeColor = Color.Red;
                formIsValidate = false;
            }

            return formIsValidate;
        }
    }
}
