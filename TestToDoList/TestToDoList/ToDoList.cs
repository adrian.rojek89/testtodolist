﻿using DatabaseContext.Model;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TestToDoList.Controls;

namespace TestToDoList
{
    public partial class ToDoList : Form
    {
        private ToDoListViewModel _mainViewModel = new ToDoListViewModel();

        public ToDoList()
        {
            InitializeComponent();
            _mainViewModel.CreateUserTaskUnderCategory = CreateUserTaskUnderCategory;
            UpdateView();
        }

        private void UpdateView()
        {
            flpMain.Controls.Clear();
            List<Category> categorys = _mainViewModel.GetAllCategories();

            foreach (Category category in categorys)
            {
                _mainViewModel.CreateCategoryControl(category, flpMain);
            }
        }

        private void btnCreateCategory_Click(object sender, System.EventArgs e)
        {
            _mainViewModel.CreateNewCategoryControl(flpMain);
        }

        private void CreateUserTaskUnderCategory(UserTask userTask)
        {
            CategoryControl categoryControl =
               flpMain.Controls.OfType<CategoryControl>().FirstOrDefault(t => t.CategoryId == userTask.CategoryId);

            if(categoryControl != null)
            {
                categoryControl.CreateUserTaskUnderThisCategory(userTask);
            }
        }
    }
}
