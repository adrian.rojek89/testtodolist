﻿using DatabaseContext.Model;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseContext.Repository
{
    public class UserTaskRepository : IUserTaskRepository
    {
        TodoDbContext _context = new TodoDbContext();

        public UserTask GetById(int id)
        {
            return _context.UserTasks.FirstOrDefault(c => c.Id == id);
        }

        public List<UserTask> GetAll()
        {
            return _context.UserTasks.ToList();
        }

        public bool Create(UserTask userTask)
        {
            _context.UserTasks.Add(userTask);
            return SaveOperation();
        }

        public bool Delete(int id)
        {
            UserTask userTaskToDelete = GetById(id);

            if (userTaskToDelete != null)
            {
                _context.UserTasks.Remove(userTaskToDelete);
                return SaveOperation();
            }
            else
            {
                return false;
            }
        }

        public bool Update(UserTask userTask)
        {
            UserTask userTaskToUpdate = GetById(userTask.Id);

            if (userTaskToUpdate != null)
            {
                _context.Entry(userTaskToUpdate).CurrentValues.SetValues(userTask);
                return SaveOperation();
            }
            else
            {
                return false;
            }
        }

        private bool SaveOperation()
        {
            int operation = _context.SaveChanges();
            return operation > 0;
        }
    }

    public interface IUserTaskRepository
    {
        UserTask GetById(int id);
        List<UserTask> GetAll();
        bool Create(UserTask userTask);
        bool Delete(int id);
        bool Update(UserTask userTask);
    }
}
