﻿using DatabaseContext.Model;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseContext.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        TodoDbContext _context = new TodoDbContext();

        public Category GetById(int id)
        {
            return _context.Categorys.FirstOrDefault(c => c.Id == id);
        }

        public List<Category> GetAll()
        {
            return _context.Categorys.ToList();
        }

        public bool Create(Category category)
        {
            _context.Categorys.Add(category);
            return SaveOperation();
        }

        public bool Delete(int id)
        {
            Category categoryToDelete = GetById(id);

            if(categoryToDelete != null)
            {
                _context.Categorys.Remove(categoryToDelete);
                return SaveOperation();
            }
            else
            {
                return false;
            }
        }

        public bool Update(Category category)
        {
            Category categoryToUpdate = GetById(category.Id);

            if (categoryToUpdate != null)
            {
                _context.Entry(categoryToUpdate).CurrentValues.SetValues(category);
                return SaveOperation();
            }
            else
            {
                return false;
            }
        }

        public bool UpdatePosition(List<Category> categorys)
        {
            bool oneRecordWasUpdate = false;

            foreach (Category category in categorys)
            {
                Category categoryToUpdate = GetById(category.Id);

                if (categoryToUpdate != null)
                {
                    oneRecordWasUpdate = true;
                    _context.Entry(categoryToUpdate).CurrentValues.SetValues(category);
                }
            }

            if(oneRecordWasUpdate)
            {
                return SaveOperation();
            }
            else
            {
                return false;
            }
        }

        private bool SaveOperation()
        {
            int operation = _context.SaveChanges();
            return operation > 0;
        }
    }

    public interface ICategoryRepository
    {
        Category GetById(int id);
        List<Category> GetAll();
        bool Create(Category category);
        bool Delete(int id);
        bool Update(Category category);
        bool UpdatePosition(List<Category> categorys);
    }
}
