﻿using DatabaseContext.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseContext
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext() : base("name=con") { }

        public DbSet<UserTask> UserTasks { get; set; }
        public DbSet<Category> Categorys { get; set; }
    }
}
