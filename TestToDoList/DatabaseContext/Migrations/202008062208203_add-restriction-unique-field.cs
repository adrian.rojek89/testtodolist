﻿namespace DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addrestrictionuniquefield : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Categories", "PlaceId", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Categories", new[] { "PlaceId" });
        }
    }
}
